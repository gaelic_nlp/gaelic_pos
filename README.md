# README #

### What is this repository for? ###

This repository is for anyone who would like to use a Scottish Gaelic part-of-speech tagger or tokeniser, or contribute to the project. 

In order to run the tagger, you will need to have Python 3 installed. The software incorporates open-source Python modules developed for NLTK 3.0 (Natural Language Toolkit: http://www.nltk.org). 

If you are interested in training your own tagger, you can use the input files in the 'Training_file' folder. These are provided in csv format. A tagged corpus of Scottish Gaelic (ARCOSG), in Brown format ('/'), is also available at the following URL:

http://www.research.ed.ac.uk/portal/en/datasets/annotated-reference-corpus-of-scottish-gaelic-arcosg(d27fba8e-9247-480e-ad83-97b4b7cef72c).html 

See the AboutFile.txt for more information.

* The current version is 16 June 2016.

### How do I get set up? ###

To get started, clone the repo using the repo using the ‘Clone’ button on the left. Alternatively, click on ‘Downloads’, then ‘Download repository’ to get a .zip version.

To run the code from your machine, create a directory folder. In this folder, place the ScG_Tagger.py file, the Data folder (with the various files) and the Model folder, with the 2 model files.

Note: Requires Tkinter

### Who do I talk to? ###

* Dr Will Lamb (University of Edinburgh)
* w.lamb@ed.ac.uk